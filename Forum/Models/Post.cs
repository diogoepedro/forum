﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace Forum.Models
{
    public class Post
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid Id { get; set; }

        [Required]
        public string? Title { get; set; }

        public Guid UserId { get; set; }
        [JsonIgnore]
        public User? User { get; set; }

        public int CategoryId { get; set; }

        [JsonIgnore]
        public Category? Category { get; set; }

        [Required]
        public string? Body { get; set; }

        public Guid PostsThreadId { get; set; }
        [JsonIgnore]
        public PostsThread? PostsThread { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime CreationDate { get; set; }
        public bool IsPublic { get; set; }
    }
}
