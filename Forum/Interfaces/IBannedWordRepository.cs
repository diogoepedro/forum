﻿using Forum.Models;

namespace Forum.Interfaces
{
    public interface IBannedWordRepository
    {
        public Task<ICollection<BannedWord>> GetBannedWords();
        public Task<BannedWord?> GetBannedWordById(Guid id);
        public Task CreateBannedWord(BannedWord bannedWord);
        public Task DeleteBannedWord(Guid id);
    }
}
