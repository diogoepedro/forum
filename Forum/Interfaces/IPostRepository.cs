﻿using Forum.Models;

namespace Forum.Interfaces
{
    public interface IPostRepository
    {
        public Task<ICollection<Post>> GetPosts();
        public Task<Post?> GetPostById(Guid id);
        public Task CreatePost(Post post);
        public Task UpdatePost(Post post);
        public Task DeletePost(Guid id);
    }
}
