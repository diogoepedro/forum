﻿using Forum.Models;

namespace Forum.Interfaces
{
    public interface IUserRepository
    {
        public Task<ICollection<User>> GetUsers();
        public Task<User?> GetUserById(Guid id);
        public Task CreateUser(User user);
        public Task UpdateUser(User user);
        public Task DeleteUser(Guid id);
    }
}
