﻿using Forum.Models;

namespace Forum.Interfaces
{
    public interface ICategoryRepository
    {
        public Task<ICollection<Category>> GetCategories();
    }
}
