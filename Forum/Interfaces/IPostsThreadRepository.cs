﻿using Forum.Models;

namespace Forum.Interfaces
{
    public interface IPostsThreadRepository
    {
        public Task<ICollection<PostsThread>> GetPostsThreads();
        public Task<PostsThread?> GetPostsThreadById(Guid id);
        public Task CreatePostsThread(PostsThread postsThread);
    }
}
