﻿using Forum.Models;

namespace Forum.Interfaces
{
    public interface IRoleRepository
    {
        public Task<ICollection<Role>> GetRoles();
    }
}
