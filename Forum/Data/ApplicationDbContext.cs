﻿using Forum.Models;
using Microsoft.EntityFrameworkCore;

namespace Forum.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Post>()
                .Property(c => c.CreationDate)
                .HasDefaultValueSql("getdate()");
            modelBuilder.Entity<User>()
                .Property(c => c.CreationDate)
                .HasDefaultValueSql("getdate()");
        }

        public DbSet<BannedWord>? BannedWords { get; set; }
        public DbSet<Post>? Posts { get; set; }
        public DbSet<PostsThread>? PostsThreads { get; set; }
        public DbSet<User>? Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Category> Categories { get; set; }
    }
}
