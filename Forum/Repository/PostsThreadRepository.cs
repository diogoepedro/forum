﻿using Forum.Data;
using Forum.Interfaces;
using Forum.Models;
using Microsoft.EntityFrameworkCore;

namespace Forum.Repository
{
    public class PostsThreadRepository : IPostsThreadRepository
    {
        private readonly ApplicationDbContext _context;

        public PostsThreadRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ICollection<PostsThread>> GetPostsThreads()
        {
            if (_context.PostsThreads is null)
            {
                return new List<PostsThread>();
            }

            return await _context.PostsThreads.ToListAsync();
        }

        public async Task<PostsThread?> GetPostsThreadById(Guid id) =>
            await _context.PostsThreads!.FirstOrDefaultAsync(p => p.Id == id);

        public async Task CreatePostsThread(PostsThread postsThread)
        {
            _context.PostsThreads!.Add(postsThread);
            await _context.SaveChangesAsync();
        }
    }
}
