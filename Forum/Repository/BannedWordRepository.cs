﻿using Forum.Data;
using Forum.Interfaces;
using Forum.Models;
using Microsoft.EntityFrameworkCore;

namespace Forum.Repository
{
    public class BannedWordRepository : IBannedWordRepository
    {
        private readonly ApplicationDbContext _context;

        public BannedWordRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ICollection<BannedWord>> GetBannedWords()
        {
            if (_context.BannedWords is null)
            {
                return new List<BannedWord>();
            }

            return await _context.BannedWords.ToListAsync();
        }

        public async Task<BannedWord?> GetBannedWordById(Guid id) =>  
            await _context.BannedWords!.FirstOrDefaultAsync(b => b.Id == id);

        public async Task CreateBannedWord(BannedWord bannedWord)
        {
            _context.BannedWords!.Add(bannedWord);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteBannedWord(Guid id)
        {
            var bannedWord = await GetBannedWordById(id);

            _context.BannedWords.Remove(bannedWord);
            await _context.SaveChangesAsync();
        }
    }
}
