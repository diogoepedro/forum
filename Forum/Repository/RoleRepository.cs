﻿using Forum.Data;
using Forum.Interfaces;
using Forum.Models;
using Microsoft.EntityFrameworkCore;

namespace Forum.Repository
{
    public class RoleRepository : IRoleRepository
    {
        private readonly ApplicationDbContext _context;

        public RoleRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ICollection<Role>> GetRoles()
        {
            if (_context.Roles is null)
            {
                return new List<Role>();
            }

            return await _context.Roles.ToListAsync();
        }
    }
}
