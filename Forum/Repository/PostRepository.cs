﻿using Forum.Data;
using Forum.Interfaces;
using Forum.Models;
using Microsoft.EntityFrameworkCore;

namespace Forum.Repository
{
    public class PostRepository : IPostRepository
    {
        private readonly ApplicationDbContext _context;

        public PostRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ICollection<Post>> GetPosts()
        {
            if (_context.Posts is null)
            {
                return new List<Post>();
            }

            return await _context.Posts.ToListAsync();
        }

        public async Task<Post?> GetPostById(Guid id) => 
            await _context.Posts!.FirstOrDefaultAsync(p => p.Id == id);

        public async Task CreatePost(Post post)
        {
            _context.Posts!.Add(post);
            await _context.SaveChangesAsync();
        }

        public async Task UpdatePost(Post post)
        {
            _context.Entry(post).State = EntityState.Modified;
            await _context.SaveChangesAsync();

        }

        public async Task DeletePost(Guid id)
        {
            var post = await GetPostById(id);

            _context.Posts.Remove(post);
            await _context.SaveChangesAsync();
        }
    }
}
