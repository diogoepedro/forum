﻿using Forum.Data;
using Forum.Interfaces;
using Forum.Models;
using Microsoft.EntityFrameworkCore;

namespace Forum.Repository
{
    public class UserRepository : IUserRepository
    {
        private readonly ApplicationDbContext _context;

        public UserRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ICollection<User>> GetUsers()
        {
            if (_context.Users is null)
            {
                return new List<User>();
            }

            return await _context.Users.ToListAsync();
        }

        public async Task<User?> GetUserById(Guid id) =>
            await _context.Users!.FirstOrDefaultAsync(u => u.Id == id);

        public async Task CreateUser(User user)
        {
            _context.Users!.Add(user);
            await _context.SaveChangesAsync();
        }
        public async Task UpdateUser(User user)
        {
            _context.Entry(user).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
        public async Task DeleteUser(Guid id)
        {
            var user = await GetUserById(id);

            _context.Users.Remove(user);
            await _context.SaveChangesAsync();
        }
    }
}
