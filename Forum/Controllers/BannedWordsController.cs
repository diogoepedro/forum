﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Forum.Data;
using Forum.Models;
using Forum.Interfaces;

namespace Forum.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BannedWordsController : ControllerBase
    {
        private readonly IBannedWordRepository _bannedWordRepository;

        public BannedWordsController(IBannedWordRepository bannedWordRepository)
        {
            _bannedWordRepository = bannedWordRepository;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<BannedWord>>> GetBannedWords()
        {
            return Ok(await _bannedWordRepository.GetBannedWords());
        }
        
        [HttpGet("{id}")]
        public async Task<ActionResult<BannedWord>> GetBannedWord(Guid id)
        {
            var bannedWord = await _bannedWordRepository.GetBannedWordById(id);

            if (bannedWord is null)
            {
                return NotFound();
            }

            return Ok(bannedWord);
        }

        [HttpPost]
        public async Task<ActionResult<BannedWord>> PostBannedWord(BannedWord bannedWord)
        {
            await _bannedWordRepository.CreateBannedWord(bannedWord);

            return Ok(CreatedAtAction("GetBannedWord", new { id = bannedWord.Id }, bannedWord));
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBannedWord(Guid id)
        {
            await _bannedWordRepository.DeleteBannedWord(id);

            return NoContent();
        }
    }
}
