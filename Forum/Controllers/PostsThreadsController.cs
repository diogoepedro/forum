﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Forum.Data;
using Forum.Models;
using Forum.Interfaces;

namespace Forum.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostsThreadsController : ControllerBase
    {
        private readonly IPostsThreadRepository _postsThreadRepository;

        public PostsThreadsController(IPostsThreadRepository postsThreadRepository)
        {
            _postsThreadRepository = postsThreadRepository;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<PostsThread>>> GetPostsThreads()
        {
            return Ok(await _postsThreadRepository.GetPostsThreads());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<PostsThread>> GetPostsThread(Guid id)
        {
            var postsThread = await _postsThreadRepository.GetPostsThreadById(id);

            if (postsThread == null)
            {
                return NotFound();
            }

            return Ok(postsThread);
        }

        [HttpPost]
        public async Task<ActionResult<PostsThread>> PostPostsThread(PostsThread postsThread)
        {
            await _postsThreadRepository.CreatePostsThread(postsThread);

            return Ok(CreatedAtAction("GetPostsThread", new { id = postsThread.Id }, postsThread));
        }
    }
}
