﻿using FakeItEasy;
using FluentAssertions;
using Forum.Controllers;
using Forum.Interfaces;
using Forum.Models;
using Microsoft.AspNetCore.Mvc;

namespace Forum.Tests.Controllers.Tests
{
    public class BannedWordsControllerTests
    {
        private IBannedWordRepository _repository;
        private BannedWordsController? _controller;

        [SetUp]
        public void SetUp()
        {
            _repository = A.Fake<IBannedWordRepository>();
        }

        [Test]
        public async Task GetBannedWords_ReturnsOk()
        {
            var bannedWordsList = A.Fake<List<BannedWord>>();
            A.CallTo(() => _repository.GetBannedWords()).Returns(bannedWordsList);
            _controller = new BannedWordsController(_repository);

            var result = await _controller.GetBannedWords();

            result.Should().NotBeNull();
            result.Result.Should().BeOfType(typeof(OkObjectResult));
        }

        [Test]
        public async Task GetBannedWordById_ReturnsOk()
        {
            var bannedWord = A.Fake<BannedWord>();
            A.CallTo(() => _repository.GetBannedWordById(bannedWord.Id)).Returns(bannedWord);
            _controller = new BannedWordsController(_repository);

            var result = await _controller.GetBannedWord(bannedWord.Id);

            result.Should().NotBeNull();
            result.Result.Should().BeOfType(typeof(OkObjectResult));
        }

        [Test]
        public async Task PostBannedWord_ReturnsOk()
        {
            var bannedWord = A.Fake<BannedWord>();
            A.CallTo(() => _repository.CreateBannedWord(bannedWord));
            _controller = new BannedWordsController(_repository);

            var result = await _controller.PostBannedWord(bannedWord);

            result.Should().NotBeNull();
            result.Result.Should().BeOfType(typeof(OkObjectResult));
        }

        [Test]
        public async Task DeleteBannedWord_ReturnsNoContent()
        {
            var bannedWord = A.Fake<BannedWord>();
            A.CallTo(() => _repository.DeleteBannedWord(bannedWord.Id));
            _controller = new BannedWordsController(_repository);

            var result = await _controller.DeleteBannedWord(bannedWord.Id);

            result.Should().NotBeNull();
            result.Should().BeOfType(typeof(NoContentResult));
        }
    }
}
