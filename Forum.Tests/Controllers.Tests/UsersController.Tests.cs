﻿using FakeItEasy;
using FluentAssertions;
using Forum.Controllers;
using Forum.Interfaces;
using Forum.Models;
using Microsoft.AspNetCore.Mvc;

namespace Forum.Tests.Controllers.Tests
{
    public class UsersControllerTests
    {
        private IUserRepository _repository;
        private UsersController _controller;

        [SetUp]
        public void SetUp()
        {
            _repository = A.Fake<IUserRepository>();
        }

        [Test]
        public async Task GetUsers_ReturnsOk()
        {
            var usersList = A.Fake<List<User>>();
            A.CallTo(() => _repository.GetUsers()).Returns(usersList);
            _controller = new UsersController(_repository);

            var result = await _controller.GetUsers();

            result.Should().NotBeNull();
            result.Result.Should().BeOfType(typeof(OkObjectResult));
        }

        [Test]
        public async Task GetUserById_ReturnsOk()
        {
            var user = A.Fake<User>();
            A.CallTo(() => _repository.GetUserById(user.Id)).Returns(user);
            _controller = new UsersController(_repository);

            var result = await _controller.GetUser(user.Id);

            result.Should().NotBeNull();
            result.Result.Should().BeOfType(typeof(OkObjectResult));
        }

        [Test]
        public async Task PostUser_ReturnsOk()
        {
            var user = A.Fake<User>();
            A.CallTo(() => _repository.CreateUser(user));
            _controller = new UsersController(_repository);

            var result = await _controller.PostUser(user);

            result.Should().NotBeNull();
            result.Result.Should().BeOfType(typeof(OkObjectResult));
        }

        [Test]
        public async Task PutUser_ReturnsNoCotent()
        {
            var user = A.Fake<User>();
            A.CallTo(() => _repository.UpdateUser(user));
            _controller = new UsersController(_repository);

            var result = await _controller.PutUser(user.Id, user);

            result.Should().NotBeNull();
            result.Should().BeOfType(typeof(NoContentResult));
        }

        [Test]
        public async Task DeleteBannedWord_ReturnsNoContent()
        {
            var user = A.Fake<User>();
            A.CallTo(() => _repository.DeleteUser(user.Id));
            _controller = new UsersController(_repository);

            var result = await _controller.DeleteUser(user.Id);

            result.Should().NotBeNull();
            result.Should().BeOfType(typeof(NoContentResult));
        }
    }
}
