﻿using FakeItEasy;
using FluentAssertions;
using Forum.Controllers;
using Forum.Interfaces;
using Forum.Models;
using Microsoft.AspNetCore.Mvc;

namespace Forum.Tests.Controllers.Tests
{
    internal class PostsThreadsControllerTests
    {
        private IPostsThreadRepository _repository;
        private PostsThreadsController? _controller;

        [SetUp]
        public void SetUp()
        {
            _repository = A.Fake<IPostsThreadRepository>();
        }

        [Test]
        public async Task GetPostsThreads_ReturnsOk()
        {
            var postsThreadsList = A.Fake<List<PostsThread>>();
            A.CallTo(() => _repository.GetPostsThreads()).Returns(postsThreadsList);
            _controller = new PostsThreadsController(_repository);

            var result = await _controller.GetPostsThreads();

            result.Should().NotBeNull();
            result.Result.Should().BeOfType(typeof(OkObjectResult));
        }

        [Test]
        public async Task GetPostsThreadById_ReturnsOk()
        {
            var postsThread = A.Fake<PostsThread>();
            A.CallTo(() => _repository.GetPostsThreadById(postsThread.Id)).Returns(postsThread);
            _controller = new PostsThreadsController(_repository);

            var result = await _controller.GetPostsThread(postsThread.Id);

            result.Should().NotBeNull();
            result.Result.Should().BeOfType(typeof(OkObjectResult));
        }

        [Test]
        public async Task PostBannedWord_ReturnsOk()
        {
            var postsThread = A.Fake<PostsThread>();
            A.CallTo(() => _repository.CreatePostsThread(postsThread));
            _controller = new PostsThreadsController(_repository);

            var result = await _controller.PostPostsThread(postsThread);

            result.Should().NotBeNull();
            result.Result.Should().BeOfType(typeof(OkObjectResult));
        }
    }
}
