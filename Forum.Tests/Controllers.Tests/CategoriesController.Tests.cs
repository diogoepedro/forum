﻿using FakeItEasy;
using FluentAssertions;
using Forum.Controllers;
using Forum.Interfaces;
using Forum.Models;
using Microsoft.AspNetCore.Mvc;

namespace Forum.Tests.Controllers.Tests
{
    public class CategoriesControllerTests
    {
        private ICategoryRepository _repository;
        private CategoriesController _controller;

        [SetUp]
        public void SetUp()
        {
            _repository = A.Fake<ICategoryRepository>();
        }

        [Test]
        public async Task GetCategories_ReturnsOk()
        {
            var categories = A.Fake<List<Category>>();
            A.CallTo(() => _repository.GetCategories()).Returns(categories);
            _controller = new CategoriesController(_repository);

            var result = await _controller.GetCategories();

            result.Should().NotBeNull();
            result.Result.Should().BeOfType(typeof(OkObjectResult));
        }
    }
}
