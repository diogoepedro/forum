﻿using FakeItEasy;
using FluentAssertions;
using Forum.Controllers;
using Forum.Interfaces;
using Forum.Models;
using Microsoft.AspNetCore.Mvc;

namespace Forum.Tests.Controllers.Tests
{
    public class PostsControllerTests
    {
        private IPostRepository _repository;
        private PostsController _controller;

        [SetUp]
        public void SetUp()
        {
            _repository = A.Fake<IPostRepository>();
        }

        [Test]
        public async Task GetPosts_ReturnsOk()
        {
            var postsList = A.Fake<List<Post>>();
            A.CallTo(() => _repository.GetPosts()).Returns(postsList);
            _controller = new PostsController(_repository);

            var result = await _controller.GetPosts();

            result.Should().NotBeNull();
            result.Result.Should().BeOfType(typeof(OkObjectResult));
        }

        [Test]
        public async Task GetPostById_ReturnsOk()
        {
            var post = A.Fake<Post>();
            A.CallTo(() => _repository.GetPostById(post.Id)).Returns(post);
            _controller = new PostsController(_repository);

            var result = await _controller.GetPost(post.Id);

            result.Should().NotBeNull();
            result.Result.Should().BeOfType(typeof(OkObjectResult));
        }

        [Test]
        public async Task PostPost_ReturnsOk()
        {
            var post = A.Fake<Post>();
            A.CallTo(() => _repository.CreatePost(post));
            _controller = new PostsController(_repository);

            var result = await _controller.PostPost(post);

            result.Should().NotBeNull();
            result.Result.Should().BeOfType(typeof(OkObjectResult));
        }

        [Test]
        public async Task PutPost_ReturnsNoCotent()
        {
            var post = A.Fake<Post>();
            A.CallTo(() => _repository.UpdatePost(post));
            _controller = new PostsController(_repository);

            var result = await _controller.PutPost(post.Id, post);

            result.Should().NotBeNull();
            result.Should().BeOfType(typeof(NoContentResult));
        }

        [Test]
        public async Task DeletePost_ReturnsNoContent()
        {
            var post = A.Fake<Post>();
            A.CallTo(() => _repository.DeletePost(post.Id));
            _controller = new PostsController(_repository);

            var result = await _controller.DeletePost(post.Id);

            result.Should().NotBeNull();
            result.Should().BeOfType(typeof(NoContentResult));
        }
    }
}
