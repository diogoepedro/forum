﻿using FakeItEasy;
using FluentAssertions;
using Forum.Controllers;
using Forum.Interfaces;
using Forum.Models;
using Microsoft.AspNetCore.Mvc;

namespace Forum.Tests.Controllers.Tests
{
    public class RolesControllerTests
    {
        private IRoleRepository _repository;
        private RolesController? _controller;

        [SetUp]
        public void SetUp()
        {
            _repository = A.Fake<IRoleRepository>();
        }

        [Test]
        public async Task GetRoles_ReturnsOk()
        {
            var roles = A.Fake<List<Role>>();
            A.CallTo(() => _repository.GetRoles()).Returns(roles);
            _controller = new RolesController(_repository);

            var result = await _controller.GetRoles();

            result.Should().NotBeNull();
            result.Result.Should().BeOfType(typeof(OkObjectResult));
        }
    }
}
